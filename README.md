# Lernfeld5

## Team 5

| Name               | GitLab                                                 |
| ------------------ | ------------------------------------------------------ |
| Yannick Mohr       | [@Langenhornmarkt](https://gitlab.com/Langenhornmarkt) |
| Nico Kossenjans    | [@ZuDummZumFeuern](https://gitlab.com/ZuDummZumFeuern) |
| Denniss Gorsantovs | [@Grauian](https://gitlab.com/Grauian)                 |
| Yannik Peter       | [@Eulentier161](https://gitlab.com/Eulentier161)       |

## Arbeitsauftrag

Wir haben uns für <u>**Szenario A: Datenbank für Bio-Kochbox**</u> entschieden

## ERD

![ERD](./img/ERD.png)

```mermaid
---
title: ERD
---
erDiagram
    Address {
        int id PK
        varchar street
        int house_number
        varchar zip
        varchar city
        varchar telephone_number
        varchar email
    }
    Supplier {
        int id PK
        varchar name
    }
    Customer {
        int id PK
        varchar last_name
        varchar first_name
        date birth_date
    }
    Order {
        int id PK
        int customer_id
        date date
        decimal invoiced_amount
    }
    Ingredient {
        int id PK
        int supplier_id
        varchar name
        varchar unit
        decimal price
        int stock
    }
    NutritionalCategories {
        int id PK
        varchar name
    }
    Recipe {
        int id PK
        varchar name
    }
    Allergenic {
        int id PK
        varchar name
    }
    Nutrition {
        int id PK
        int ingredient_id
        decimal calories
        decimal protein
        decimal carbonhydrates
        decimal fat 
        decimal fiber
        decimal sodium
    }

    Address ||--|| Customer : is
    Address ||--|| Supplier : is
    Customer ||--o{ Order : places
    Order ||--|{ Ingredient : contains
    Recipe ||--|{ Ingredient : contains
    Ingredient ||--|| Nutrition : has 
    Recipe }|--|| NutritionalCategories : is
    Recipe }|--|| Allergenic : is
```

# Szenario A: Datenbank für Bio-Kochbox

## Aufgabenstellung

Ausgehend von den bereits im Produktportfolio befindlichen Bio-Lebensmitteln soll ein Rezept-Service mit darauf angepasster Rezept-Box eingeführt werden. Darüber hinaus soll die Option bestehen, Rezepte anhand bestimmter Ernährungskategorien sowie Beschränkungen aufgrund von Allergenen / Unverträglichkeiten zu filtern.\
Die Boxen sollen dann später entsprechend der in den Rezepten verwendeten Zutaten und Mengen bestückt werden.

**Erweitern Sie die bestehende Datenbank dahingehend, dass die gewünschten Funktionen unterstützt werden.**\
**Hinweis: Die Entwicklung und Anbindung einer grafischen Benutzeroberfläche ist nicht zwingend Teil dieser Aufgabe, kann - bei entsprechenden Vorkenntnissen - jedoch optional erfolgen.**

---

## Umzusetzende Kundenanforderungen:

- Speicherung von Rezepten bestehend aus mehreren Zutaten
- Speicherung von Ernährungskategorien
- Speicherung von Beschränkungen/ Allergenen
- Zusammenstellung von Zutaten entsprechend eines Rezepts
- Auswahl von Rezepten entsprechend vorgegebener Ernährungskategorien
- Auswahl bzw. Ausschluss von Rezepten auf Basis von Beschränkungen
- optional: Zugriffskontrolle personenbezogener Daten

Die Kraut und Rüben bittet Sie darüber hinaus ein Konzept zu entwickeln für die Fälle,

- dass ein Kunde Auskunft seiner Daten nach DSGVO beantragt oder
- die Löschung seiner Daten verlangt.

Klären Sie für beide Fälle sowohl die rechtlichen Grundlagen und implementieren Sie die technische Umsetzung (Beispielsweise als SQL-Skript).

| Beispielhafte Ernährungskategorien | Beschränkungen |
| ---------------------------------- | -------------- |
| - Vegan                            | - Laktose      |
| - Vegetarisch                      | - Gluten       |
| - Frutarisch                       | - Erdnuss      |
| - Low Carb                         | - Ei           |
| - High Protein                     | - Tomate       |
| - ...                              | - ...          |

| Nährstoffangaben              | pro 100 g       |
| ----------------------------- | --------------- |
| Kalorien                      | 2000kj/ 478kcal |
| Proteine                      | 2,1g            |
| Kohlenhydrate                 | 77,2g           |
| - davon Zucker                | 12,4g           |
| Fett                          | 17,9g           |
| - davon gesättigte Fettsäuren | 13,9g           |
| Ballaststoffe                 | 6,2g            |
| Natrium                       | 0,03g           |

## Anforderungen an die von Ihnen zu entwickelnden Abfragen:

- Setzen Sie die folgenden Anforderungen mittels SQL-Abfragen um:
  - Auswahl aller Zutaten eines Rezeptes nach Rezeptname
  - Auswahl aller Rezepte einer bestimmten Ernährungskategorie
  - Auswahl aller Rezepte, die eine gewisse Zutat enthalten
  - Berechnung der durchschnittlichen Nährwerte aller Bestellungen eines Kunden
  - Auswahl aller Zutaten, die bisher keinem Rezept zugeordnet sind
- optional für FiSi und ITSE bzw. verpflichtend für FIAE und FIDP:
  - Auswahl aller Rezepte, die eine bestimmte Kalorienmenge nicht überschreiten
  - Auswahl aller Rezepte, die weniger als fünf Zutaten enthalten
  - Auswahl aller Rezepte, die weniger als fünf Zutaten enthalten und eine bestimmte Ernährungskategorie erfüllen
- Erstellen Sie mindestens drei weitere Abfragen
- Stellen Sie sicher, dass Sie insgesamt mindestens je eine Abfrage mit **inner join**, **left join**/**right join** sowie **Subselects** und **Aggregatfunktionen** erstellen

---

## Empfohlenes Vorgehen

![Lernlandkarte](./img/Lernlandkarte.jpg)

Erklärung zu den einzelnen Schritten:

1.  **Finde ein Team**

    - Entscheiden Sie, ob Sie alleine, mit einer/m Partner/in oder in einem Dreier-Team arbeiten möchten.
    - Damit sich alle voll in das Projekt einbringen können, raten wir stark von Teams mit mehr als drei Mitgliedern ab. Bilden Sie am besten interdisziplinäre Teams, mit Auszubildenden aus den verschiedenen Fachrichtungen.
    - Entscheiden Sie, wer welche Scrum-Rolle übernimmt.
    - Definieren Sie Teamregeln und schreiben Sie diese auf. (Nach welchen Regeln treffen Sie z. B. Entscheidungen, welche Kommunikationswege legen Sie fest und welche Regeln formulieren Sie für Konflikte.)
    - Lernen Sie miteinander und voneinander. Unterstützen Sie sich im Team und auch über die Teams hinaus.

2.  **Klarheit schaffen**

    - Ihr Lehrerteam wird bzw. hat Ihnen bereits eine Einführung in das Modul geben.
    - Sichten Sie die Kursmaterialien.
    - Besprechen Sie im Team, wie Sie den Projektauftrag verstehen. Haben Sie alle das gleiche Verständnis?
    - Schreiben Sie alle Unklarheiten auf und klären Sie diese mit Ihrem Lehrerteam ab.
    - Einigen Sie sich auf einen Kompetenznachweis, den Sie in diesem Lernfeld erbringen möchten und der bewertet werden soll. Sie können dem Lehrerteam auch gerne einen alternativen Kompetenznachweis vorschlagen.

3.  **Vorbereitung**

    - Bevor Sie loslegen, richten Sie sich Ihre Arbeitsumgebung ein. Für dieses Lernfeld erwarten wir, dass Sie ein digitales Scrum-Board nutzen. Digitale Scrum-Boards gibt es online und kostenlos. Sie können zum Beispiel [Trello](https://trello.com/de) oder [Meistertask](https://www.meistertask.com/de) für Ihr digitales Scrum-Board nutzen. Sprechen Sie sich mit dem Lehrerteam ab, wie diese Ihr Scrum-Board einsehen können. Darüber hinaus wird die Nutzung von GitLab zur Speicherung und Versionierung Ihrer Dateien empfohlen bzw. von FIAE erwartet. Eine kurze Einführung zur Arbeit mit GitLab finden Sie [hier](https://moodle.itech-bs14.de/course/view.php?id=570#section-3).

4.  **Backlog füllen**

    - Tragen Sie in Ihr Projekt-Backlog alle Aufgaben ein, die Sie zum jetzigen Zeitpunkt für das gesamte Projekt erkennen können. Das Backlog ist keine statische "Aufgabenliste". Es ist erlaubt, dass im Laufe des Projekts noch weitere Aufgaben dazu kommen oder wegfallen. Wichtig, nehmen Sie neben den technischen Aufgaben auch alle Aufgaben aus dem Bereich Recherche, Information und Austausch mit auf.

5.  **Sprint 1 bis n**
6.  **Kompetenznachweis**

---

## Vorschlag für die Sprint Backlogs:

**Das hier vorgeschlagene Vorgehen bezieht sich auf die technischen Arbeitspakete des Kundenauftrag. Nehmen Sie in Ihre Sprintplanung auch die datenschutzrechtliche Aufgabe mit auf sowie die Vorbereitung der Leistung- und Kompetenznachsnachweise.**

### **Sprint 1 "Discovery Track"**

Im ersten Sprint geht es rein darum, sich über die verschiedenen Themen zu informieren. Aufgaben in diesem Sprint umfassen z. B. Selbsstudium, Lesen, Zusammenfassen, sich gegenseitig erklären, an (Lehrer-) Vorträgen teilnehmen, Fragen aufschreiben und diese klären. Das Produkt des Discovery Tracks könnte z. B. ein Dokument sein, welches die wesentlichen Erkenntnisse stichwortartig zusammenfasst. Das kann beispielsweise eine Liste der wichtigsten SQL-Befehle sein, eine Beschreibung der Teamrollen und Teamregeln oder Ihr ausformuliertes Ziel für dieses Projekt. Solche Dokumente können Sie ebenfalls in GitLab speichern.

### **Sprint 2 "Conceptual and logical Database Design"**

Zur Sprintplanung orientiert sich dieses Szenario am [Database System Development Life-Cycle](<https://github.com/edward-leafmill/2016_database/wiki/Database-System-Development-Lifecycle-(DSDLC)>). Sprint 2 ist Ihr erster richtiger Entwicklungssprint. Es geht darum, die Datenbank zu modellieren. Sie sollten drei wesentliche Tasks in Ihre Sprintplanung aufnehmen:

- Conceptual Database Design
  - Sammeln Sie alle Informationen aus dem Unternehmen, die Sie in der Datenbank verwalten wollen. Sichten und dokumentieren Sie also Beispielsweise die Kundendaten, Rezepte und Bestelldaten.
- Logical Database Design
  - Entwickeln Sie aus den gesammelten Informationen ein Entity Relationship Model (ERM).
- Normalisierung
  - Prüfen Sie Ihr ERM und wenden Sie die Normalformen auf Ihr ERM an.

Der Scrum Master ist dafür verantwortlich, dass Sie zum Abschluss des Sprints eine Product Demo und eine Team Retro durchführen (dies ist verpflichtend).\
Dies bedeutet, dass Sie sich eine Partner-Gruppe suchen und sich gegenseitig ihre ERMs vorstellen. Diskutieren Sie ob die Entitäten korrekt gebildet wurden und die Normalformen umgesetzt wurden. Stellen Sie sich insbesondere die Frage, ob Ihr ERM erweiterungsfähig ist. Müssen Entitäten beziehungsweise Tabellen abgeändert werden wenn beispielsweise eine neue Ernährungskategorie oder ein neues Allergen hinzukommt?

Wenn notwendig, überarbeiten Sie Ihr ERM im folgenden Sprint!

### **Sprint 3 "Anpassen der Datenbank und Testdaten"**

Wenn Ihr Datenbankmodel fertig ist, werden Sie die Datenbank in diesem Sprint entsprechend ändern und erste Testdaten einfügen. Hierzu sind insbesondere folgende Schritte notwendig:

- Erstellen Sie Ihre Entwicklungsumgebung.
  - Stellen Sie sicher, dass jedes Teammitglied ein funktionsfähges Datenbankmanagementsystem zur Verfügung hat.
- Importieren Sie die bestehende Datenbank des Kunden.
- Nehmen Sie die geplanten Änderungen an der Datenbank vor.
- Füllen Sie die Datenbank mit Testdaten.

Wie in jedem Sprint, gehen Sie nach dem SCRUM-Prozess vor:

- Zielbestimmung: Wie soll die Produktversion am Ende des Sprints aussehen?
- Grooming Session: Welche Aufgaben sind zur Umsetzung dafür zu bearbeiten
  - (Aufgaben werden aus dem Projekt Backlog in das Sprint Backlog gezogen)
- Entwicklungsarbeit (DB -eventuell ist eine Erweiterung des ERM notwendig)
- Product Demo (Feedback der Partnergruppe)
- Team Retro

### **Sprint 4 bis x "Entwicklung der Abfragen"**

Planen Sie in den folgenden Sprints Ihre SQL-Abfrgen. Sie können sich aus dem Kundenauftrag zunächst umgagsprachilch die Kundenanforderungen rausschreiben und dann daruas die SQL-Abfragen entwickeln.

Gehen Sie wieder entsprechend des SCRUM Prozess vor, von Zielbestimmung, Grooming Session bis zur Team Retro

Sie können beispielsweise je in einem Sprint die Features **Rezepte nach Ernährungskategorien filtern** und **Rezepte nach Allergenen filtern** umsetzen.

### **Sprint n "Continuous improvement"**

Sprintziel: Abgesehen davon, dass die ursprüngliche Datenbank die neuen Features nicht unterstützt hat, gibt es noch weiteren Optimierungsbedarf: Schauen Sie sich das ERD des ursprünglichen Ist-Zustands nochmal an. Hier sind insbesondere Verbesserungen bei der Adressspeicherung notwendig (z. B. Lieferadressen, Rechnungsadresse). Darüberhinaus wurden bisher keine Zahlungsdaten/ Zahlungsarten der Kunden gespeichert, da die Zahlung bisher ausschließlich über externe Zahlungsdienstleister abgewickelt wurde. Machen Sie auch hierfür einen geeigneten Vorschlag, um den Kunden zukünftig auch andere Zahlungsweisen ermöglichen zu können. Auch ein Abo-Modell wäre ein mögliches Geschäftsmodell für Kraut und Rüben. Seien Sie kreativ und entwickeln Sie weitere eigene Ideen, wie Sie Ihr Produkt weiter ausbauen möchten. Besprechen Sie dies mit Ihrem Lehrerteam.

Für FIAE/FIDP: Setzen Sie mindestens drei Stored Procedures in Ihrer Datenbank um.

---

## Folgende <u>technische</u> Teilhandlungen sind zur Bearbeitung des Kundenauftrags durchzuführen:

_Um für die Bearbeitung der Teilhandlungen fachlich fit zu werden, sollten sich Datenbank-Einsteiger unbedingt zunächst mit den Fachinhalten vertraut machen. Bitte bearbeiten Sie daher zuerst die jeweiligen Fachmodule der Infotheke bevor Sie in Ihrem Team die dazugehörige Teilhandlung durchführen._

| Schritt | Teilhandlung                                                                                                                                                                 | Arbeitsauftrag                                                                                                                                                                                                                                                                                                                                                                                                                                              | Fachmodul/Infotheke                                                                                                                                                                                                                                                                                                                                                                                                                 |
| ------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1       | Analyse und Anpassung des aktuellen ERD                                                                                                                                      | Schauen Sie sich den IST-Zustand des ERDs von Kraut&Rüben an und nehmen Sie die notwendigen Änderungen und Ergänzungen vor, um die Kundenwünsche erfüllen zu können.                                                                                                                                                                                                                                                                                        | - [Datentheorie](https://moodle.itech-bs14.de/course/view.php?id=858) </br> - [Einführung in agiles Projektmanagement mit Scrum](https://moodle.itech-bs14.de/course/view.php?id=980) </br> - [Glossar: Datenbank-Grundbegriffe](https://moodle.itech-bs14.de/course/view.php?id=763) </br> - [Entity Relationship Diagram (ERD)](https://moodle.itech-bs14.de/course/view.php?id=763)                                              |
| 2       | Realisierung der Änderungen in der bestehenden Datenbank unter Beachtung der Normalisierungsvorschriften                                                                     | Überführen Sie das zuvor angepasste ERD in das Relationenschema. Achten Sie darauf, dass die Tabellen mindestens in der 3. Normalform vorliegen. </br> Importieren Sie anschließend die bestehende Datenbank und setzen Sie die Änderungen an der Struktur der Datenbank durch entsprechende SQL-Befehle um.                                                                                                                                                | - [Tabellen aus einem ERD ableiten](https://moodle.itech-bs14.de/mod/page/view.php?id=32483) </br> - [Normalisierung](https://moodle.itech-bs14.de/course/view.php?id=566) </br> - [Datentypen](https://moodle.itech-bs14.de/mod/page/view.php?id=30307&inpopup=1) </br> - [Data Definition Language (DDL)](https://moodle.itech-bs14.de/course/view.php?id=560)                                                                    |
| 3       | Einpflegen der bereitgestellten ergänzenden Daten in die angepasste Datenstruktur                                                                                            | Fügen Sie die Daten aus den zur Verfügung gestellten Rezepten in die geänderten bzw. neuen Tabellen ein.                                                                                                                                                                                                                                                                                                                                                    | - INSERT- bzw. UPDATE Befehl der [Data Manipulation Lanuage (DML)](https://moodle.itech-bs14.de/mod/page/view.php?id=36675&inpopup=1)                                                                                                                                                                                                                                                                                               |
| 4       | Entwickeln von Testfällen anhand der Kundenanforderungen, Abfragen erstellen und Abgleich mit erwarteten Ergebnissen, ggf. Überarbeitung der Datenstruktur und/oder Abfragen | Testen Sie, ob die von Ihnen vorgenommenen Änderungen an der Datenbankstruktur die Anforderungen des Kunden ermöglichen. Entwickeln Sie hierzu Testfälle und realisieren Sie diese mit entsprechenden SQL Abfragen. Vergleichen Sie die Ergebnisse Ihrer Abfragen mit den erwarteten Ausgaben. Passen Sie Ihre Datenstruktur ggf. an. _Für FIAE/FIDP: Überlegen Sie, wie Sie mithilfe von Stored Procedures und Triggern Ihre Datenbank optimieren können._ | - [Data Manipulation Language](https://moodle.itech-bs14.de/course/view.php?id=561) ([Abfragen](https://moodle.itech-bs14.de/mod/page/view.php?id=32303&inpopup=1), [JOINS](https://moodle.itech-bs14.de/mod/page/view.php?id=32387&inpopup=1), [Aggregatfunktionen](https://moodle.itech-bs14.de/mod/page/view.php?id=36613&inpopup=1), [Unterabfragen](https://moodle.itech-bs14.de/mod/page/view.php?id=37312&inpopup=1), Views) |
| 5       | Erweiterung und Anbindung einer grafischen Benutzeroberfläche (_optional_)                                                                                                   | Bei entsprechenden Vorkenntnissen (PHP, HTML/ CSS, objektorientierte Programmierung mit GUI) können Sie Ihre Datenbank an ein User Interface anbinden.                                                                                                                                                                                                                                                                                                      | - [Datenbankanbindung](https://moodle.itech-bs14.de/course/view.php?id=564)                                                                                                                                                                                                                                                                                                                                                         |
| 6       | Umsetzung einer NoSQL Lösung (Erfassung von Bewegungsdaten) (_optional_)                                                                                                     | Erweitern Sie Ihre relationale Datenbanklösung optional um eine NoSQL Lösung.                                                                                                                                                                                                                                                                                                                                                                               | - [NoSQL](https://moodle.itech-bs14.de/course/view.php?id=563)                                                                                                                                                                                                                                                                                                                                                                      |

---

## Ressourcen

### Ist-Zustand der Datenbank

![IST Zustand ERD](./img/IST%20Zustand%20ERD.jpg)

### Datenbank Dump Ist-Zustand

Laden Sie die folgende [Datei](https://moodle.itech-bs14.de/pluginfile.php/58532/mod_page/content/121/sql_dump_database_create.sql) herunter und importieren Sie diese z.B. in HeidiSQL. Führen Sie anschließend folgende [Datei](https://moodle.itech-bs14.de/pluginfile.php/58532/mod_page/content/121/krautundruebenload.sql?time=1601017319776) aus, um die Datenbank mit Daten zu füllen. Eine Anleitung zu Einrichtung und Umgang mit HeidiSQL finden Sie [hier](https://moodle.itech-bs14.de/course/view.php?id=560#section-1).

### Projekt Backlog (Vorschlag)

Wichtig: Dieses beispielhafte Projekt Backlog müssen Sie in Ihrem Team konkretisieren.\
Um das Projekt Backlog übersichtlich zu halten, bietet es sich an, hier nur Features oder Stories reinzuschreiben. Es ist aber nicht verboten hier auch schon Aufgaben zu vermerken.\
Wenn Sie zu Beginn Ihrer Sprints eine Grooming-Session machen und die To Dos für Ihren Sprint festlegen, bietet es sich an bei den Features konkreter zu werden. Spätestens im Daily Scrum sollten Sie für jede Aufgabe die folgenden Fragen beantworten:

- Was wird genau gemacht?
  - Beispiel: Ungenaue Beschreibung: "Anpassung des ERD"; genauere Beschreibung: "Anpassung des ERD um Rezepte"
- Wer bearbeitet die Aufgabe?
- Bis wann bearbeitet er/sie die Aufgabe?

![LF5 Projekt-Backlog](./img/LF5%20Projekt-Backlog.jpg)
