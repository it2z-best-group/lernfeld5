USE krautundrueben;

-- Auswahl aller Zutaten eines Rezeptes nach Rezeptname
SELECT i.* FROM Ingredient i
RIGHT JOIN RecipeAmount ra
ON i.id = ra.ingredient_id
RIGHT JOIN Recipe r
ON ra.recipe_id = r.id
WHERE r.name = "<hier Rezeptname>"

-- Auswahl aller Rezepte einer bestimmten Ernährungskategorie
SELECT UNIQUE(r.name) FROM Recipe r
INNER JOIN RecipeNutritionalCategories rnc
ON rnc.recipe_id = r.id
INNER JOIN NutritionalCategories nc
ON nc.id = rnc.nutritionalcategories_id
WHERE nc.name = "<hier Ernährungskategorie>";

-- Auswahl aller Rezepte, die eine gewisse Zutat enthalten
SELECT r.name FROM Ingredient i
INNER JOIN RecipeAmount ra
ON i.id = ra.ingredient_id
INNER JOIN Recipe r 
ON ra.recipe_id = r.id
WHERE i.name = "hier Zutat";

-- Berechnung der durchschnittlichen Nährwerte aller Bestellungen eines Kunden
SELECT 
    AVG(oi.amount * n.calories) calories, 
    AVG(oi.amount * n.protein) protein, 
    AVG(oi.amount * n.carbohydrates) carbohydrates, 
    AVG(oi.amount * n.fat) fat, 
    AVG(oi.amount * n.fiber) fiber, 
    AVG(oi.amount * n.sodium) sodium 
FROM Customer c
INNER JOIN `Order` o
ON c.id = o.customer_id
INNER JOIN OrderIngredient oi
ON o.id = oi.order_id
INNER JOIN Nutrition n
ON oi.ingredient_id = n.ingredient_id
WHERE c.id = <hier customer id>;

-- Auswahl aller Zutaten, die bisher keinem Rezept zugeordnet sind
SELECT i.* FROM Ingredient i
LEFT JOIN RecipeAmount ra
ON ra.ingredient_id = i.id
WHERE ra.id IS NULL

-- Auswahl aller Rezepte, die eine bestimmte Kalorienmenge nicht überschreiten
SELECT r.name, n.calories*ra.amount calories FROM RecipeAmount ra
INNER JOIN Recipe r
ON r.id = ra.recipe_id
INNER JOIN Ingredient i
ON ra.ingredient_id = i.id
INNER JOIN Nutrition n
ON n.ingredient_id = i.id
GROUP BY r.name
HAVING calories < <hier Kalorienmenge>;

-- Auswahl aller Rezepte, die weniger als fünf Zutaten enthalten
SELECT r.name, COUNT(*) num_diff_ingredients FROM Recipe r
INNER JOIN RecipeAmount ra
ON ra.recipe_id = r.id
GROUP BY r.name
HAVING num_diff_ingredients < 5;

-- Auswahl aller Rezepte, die weniger als fünf Zutaten enthalten und eine bestimmte Ernährungskategorie erfüllen
SELECT DISTINCT(sub.name) FROM (
    SELECT r.id, r.name, COUNT(*) num_diff_ingredients FROM Recipe r
    INNER JOIN RecipeAmount ra
    ON ra.recipe_id = r.id
    GROUP BY r.name
    HAVING num_diff_ingredients < 5
) sub
INNER JOIN RecipeNutritionalCategories rnc
ON sub.id = rnc.recipe_id
INNER JOIN NutritionalCategories nc
ON nc.id = rnc.nutritionalcategories_id
WHERE nc.name = "<hier Ernährungskategorie>";

-- Auswahl aller Kunden die noch keine Bestellung getätigt haben
SELECT c.* FROM `Customer` c
LEFT JOIN `Order` o
ON c.id = o.customer_id 
WHERE o.customer_id IS NULL;

-- Auswahl einer zufälligen Zutat
SELECT * FROM Ingredient
ORDER BY RAND()
LIMIT 1
